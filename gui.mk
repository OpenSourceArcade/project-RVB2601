##
## Auto Generated makefile by CDK
## Do not modify this file, and any manual changes will be erased!!!   
##
## BuildSet
ProjectName            :=gui
ConfigurationName      :=BuildSet
WorkspacePath          :=./
ProjectPath            :=./
IntermediateDirectory  :=Obj
OutDir                 :=$(IntermediateDirectory)
User                   :=cyber
Date                   :=10/05/2022
CDKPath                :=../../C-Sky/CDK
LinkerName             :=riscv64-unknown-elf-gcc
LinkerNameoption       :=
SIZE                   :=riscv64-unknown-elf-size
READELF                :=riscv64-unknown-elf-readelf
CHECKSUM               :=crc32
SharedObjectLinkerName :=
ObjectSuffix           :=.o
DependSuffix           :=.d
PreprocessSuffix       :=.i
DisassemSuffix         :=.asm
IHexSuffix             :=.ihex
BinSuffix              :=.bin
ExeSuffix              :=.elf
LibSuffix              :=.a
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
ElfInfoSwitch          :=-hlS
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
UnPreprocessorSwitch   :=-U
SourceSwitch           :=-c 
ObjdumpSwitch          :=-S
ObjcopySwitch          :=-O ihex
ObjcopyBinSwitch       :=-O binary
OutputFile             :=$(ProjectName)
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :=gui.txt
MakeDirCommand         :=mkdir
LinkOptions            :=   -nostartfiles  -Wl,--gc-sections  -T__workspace_pack__/ch2601_evb/v7.4.3/gcc_flash.ld  -mabi=ilp32 -mtune=e906 -march=rv32imacxtheade 
LinkOtherFlagsOption   :=  -Wl,-zmax-page-size=1024 -Xlinker -Map=yoc.map
IncludePackagePath     :=
IncludeCPath           :=$(IncludeSwitch)__workspace_pack__/aos_hal/v7.4.3/include $(IncludeSwitch)__workspace_pack__/aos/v7.4.3/include $(IncludeSwitch)__workspace_pack__/aos/v7.4.3/include/devices $(IncludeSwitch)__workspace_pack__/aos/v7.4.3/src/adapter/rhino/debug/include $(IncludeSwitch)__workspace_pack__/at/v7.4.3/include $(IncludeSwitch)__workspace_pack__/ch2601_evb/v7.4.3/include $(IncludeSwitch)__workspace_pack__/chip_ch2601/v7.4.3/include $(IncludeSwitch)__workspace_pack__/chip_ch2601/v7.4.3/sys $(IncludeSwitch)__workspace_pack__/cli/v7.4.3/include/ $(IncludeSwitch)__workspace_pack__/cli/v7.4.3/ $(IncludeSwitch)__workspace_pack__/csi/v7.4.3/csi2/include $(IncludeSwitch)__workspace_pack__/drivers/v7.4.3/csi2/include $(IncludeSwitch)__workspace_pack__/drv_snd_ch2601/v7.4.3/include $(IncludeSwitch)__workspace_pack__/drv_wifi_at_w800/v7.4.3/include $(IncludeSwitch)__workspace_pack__/kv/v7.4.3/include $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/. $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/src $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/src/lv_core $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/src/lv_draw $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/src/lv_font $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/src/lv_hal $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/src/lv_misc $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/src/lv_objx $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/src/lv_themes $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/src/lv_widgets $(IncludeSwitch)__workspace_pack__/minialsa/v7.4.3/include $(IncludeSwitch)__workspace_pack__/netmgr/v7.4.3/include $(IncludeSwitch)__workspace_pack__/newlib/v7.4.3/include $(IncludeSwitch)__workspace_pack__/partition/v7.4.3/include $(IncludeSwitch)__workspace_pack__/rhino_arch/v7.4.3/include $(IncludeSwitch)__workspace_pack__/rhino_pwrmgmt/v7.4.3/include $(IncludeSwitch)__workspace_pack__/rhino/v7.4.3/include $(IncludeSwitch)__workspace_pack__/sal/v7.4.3/include $(IncludeSwitch)__workspace_pack__/sec_crypto/v7.4.3/include $(IncludeSwitch)__workspace_pack__/sec_crypto/v7.4.3/crypto/include $(IncludeSwitch)__workspace_pack__/ulog/v7.4.3/include $(IncludeSwitch)__workspace_pack__/uservice/v7.4.3/include  $(IncludeSwitch)include $(IncludeSwitch)app/include $(IncludeSwitch)app/src $(IncludeSwitch)app/src/lvgl_porting  
IncludeAPath           :=$(IncludeSwitch)__workspace_pack__/aos_hal/v7.4.3/include $(IncludeSwitch)__workspace_pack__/aos/v7.4.3/include $(IncludeSwitch)__workspace_pack__/aos/v7.4.3/include/devices $(IncludeSwitch)__workspace_pack__/aos/v7.4.3/src/adapter/rhino/debug/include $(IncludeSwitch)__workspace_pack__/at/v7.4.3/include $(IncludeSwitch)__workspace_pack__/ch2601_evb/v7.4.3/include $(IncludeSwitch)__workspace_pack__/chip_ch2601/v7.4.3/include $(IncludeSwitch)__workspace_pack__/chip_ch2601/v7.4.3/sys $(IncludeSwitch)__workspace_pack__/cli/v7.4.3/include/ $(IncludeSwitch)__workspace_pack__/cli/v7.4.3/ $(IncludeSwitch)__workspace_pack__/csi/v7.4.3/csi2/include $(IncludeSwitch)__workspace_pack__/drivers/v7.4.3/csi2/include $(IncludeSwitch)__workspace_pack__/drv_snd_ch2601/v7.4.3/include $(IncludeSwitch)__workspace_pack__/drv_wifi_at_w800/v7.4.3/include $(IncludeSwitch)__workspace_pack__/kv/v7.4.3/include $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/. $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/src $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/src/lv_core $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/src/lv_draw $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/src/lv_font $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/src/lv_hal $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/src/lv_misc $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/src/lv_objx $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/src/lv_themes $(IncludeSwitch)__workspace_pack__/lvgl/v7.4.3/src/lv_widgets $(IncludeSwitch)__workspace_pack__/minialsa/v7.4.3/include $(IncludeSwitch)__workspace_pack__/netmgr/v7.4.3/include $(IncludeSwitch)__workspace_pack__/newlib/v7.4.3/include $(IncludeSwitch)__workspace_pack__/partition/v7.4.3/include $(IncludeSwitch)__workspace_pack__/rhino_arch/v7.4.3/include $(IncludeSwitch)__workspace_pack__/rhino_pwrmgmt/v7.4.3/include $(IncludeSwitch)__workspace_pack__/rhino/v7.4.3/include $(IncludeSwitch)__workspace_pack__/sal/v7.4.3/include $(IncludeSwitch)__workspace_pack__/sec_crypto/v7.4.3/include $(IncludeSwitch)__workspace_pack__/sec_crypto/v7.4.3/crypto/include $(IncludeSwitch)__workspace_pack__/ulog/v7.4.3/include $(IncludeSwitch)__workspace_pack__/uservice/v7.4.3/include  $(IncludeSwitch)include $(IncludeSwitch)app/include $(IncludeSwitch)app/src $(IncludeSwitch)app/src/lvgl_porting  
Libs                   := -Wl,--whole-archive $(LibrarySwitch)aos_hal $(LibrarySwitch)aos $(LibrarySwitch)at $(LibrarySwitch)ch2601_evb $(LibrarySwitch)chip_ch2601 $(LibrarySwitch)cli $(LibrarySwitch)csi $(LibrarySwitch)drivers $(LibrarySwitch)drv_snd_ch2601 $(LibrarySwitch)drv_wifi_at_w800 $(LibrarySwitch)hal_csi $(LibrarySwitch)kv $(LibrarySwitch)lvgl $(LibrarySwitch)minialsa $(LibrarySwitch)netmgr $(LibrarySwitch)newlib $(LibrarySwitch)partition $(LibrarySwitch)rhino_arch $(LibrarySwitch)rhino_pwrmgmt $(LibrarySwitch)rhino $(LibrarySwitch)sal $(LibrarySwitch)sec_crypto $(LibrarySwitch)ulog $(LibrarySwitch)uservice  -Wl,--no-whole-archive  
ArLibs                 := 
PackagesLibPath        :=$(LibraryPathSwitch)__workspace_pack__/aos_hal/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/aos/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/at/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/ch2601_evb/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/chip_ch2601/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/cli/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/csi/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/drivers/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/drv_snd_ch2601/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/drv_wifi_at_w800/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/hal_csi/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/kv/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/lvgl/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/minialsa/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/netmgr/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/newlib/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/partition/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/rhino_arch/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/rhino_pwrmgmt/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/rhino/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/sal/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/sdk_chip_ch2601/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/sec_crypto/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/ulog/v7.4.3/lib $(LibraryPathSwitch)__workspace_pack__/uservice/v7.4.3/lib 
LibPath                :=$(LibraryPathSwitch)Obj  $(PackagesLibPath) 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       :=riscv64-unknown-elf-ar rcu
CXX      :=riscv64-unknown-elf-g++
CC       :=riscv64-unknown-elf-gcc
AS       :=riscv64-unknown-elf-gcc
OBJDUMP  :=riscv64-unknown-elf-objdump
OBJCOPY  :=riscv64-unknown-elf-objcopy
CXXFLAGS := $(PreprocessorSwitch)USE_AOS_HAL $(PreprocessorSwitch)USE_AOS $(PreprocessorSwitch)USE_AT $(PreprocessorSwitch)USE_CH2601_EVB $(PreprocessorSwitch)USE_CHIP_CH2601 $(PreprocessorSwitch)USE_CLI $(PreprocessorSwitch)USE_CSI $(PreprocessorSwitch)USE_DRIVERS $(PreprocessorSwitch)USE_DRV_SND_CH2601 $(PreprocessorSwitch)USE_DRV_WIFI_AT_W800 $(PreprocessorSwitch)USE_HAL_CSI $(PreprocessorSwitch)USE_KV $(PreprocessorSwitch)USE_LVGL $(PreprocessorSwitch)USE_MINIALSA $(PreprocessorSwitch)USE_NETMGR $(PreprocessorSwitch)USE_NEWLIB $(PreprocessorSwitch)USE_PARTITION $(PreprocessorSwitch)USE_RHINO_ARCH $(PreprocessorSwitch)USE_RHINO_PWRMGMT $(PreprocessorSwitch)USE_RHINO $(PreprocessorSwitch)USE_SAL $(PreprocessorSwitch)USE_SDK_CHIP_CH2601 $(PreprocessorSwitch)USE_SEC_CRYPTO $(PreprocessorSwitch)USE_ULOG $(PreprocessorSwitch)USE_USERVICE  -Wno-unused-function -Os -g -mno-ldr -mno-ext -mno-mula -mno-srri -mno-ldi -mno-addsl -mno-ldd -mno-rev -mno-condmv  -nostdlib -Wpointer-arith -Wall -Wl,-EL -ffunction-sections -fdata-sections -Os -g  $(PreprocessorSwitch)AOS_COMP_CLI=y $(PreprocessorSwitch)CLI_CONFIG_INBUF_SIZE=256 $(PreprocessorSwitch)CLI_CONFIG_MAX_ARG_NUM=16 $(PreprocessorSwitch)CLI_CONFIG_MAX_COMMANDS=64 $(PreprocessorSwitch)CLI_CONFIG_MAX_ONCECMD_NUM=4 $(PreprocessorSwitch)CLI_CONFIG_OUTBUF_SIZE=512 $(PreprocessorSwitch)CLI_CONFIG_TASK_PRIORITY=60 $(PreprocessorSwitch)CONFIG_ARCH_INTERRUPTSTACK=32768 $(PreprocessorSwitch)CONFIG_CHIP_ALKAID=1 $(PreprocessorSwitch)CONFIG_CLI=1 $(PreprocessorSwitch)CONFIG_CPU_E906=1 $(PreprocessorSwitch)CONFIG_CSI=\"csi2\" $(PreprocessorSwitch)CONFIG_CSI_V2=1 $(PreprocessorSwitch)CONFIG_DEBUG=1 $(PreprocessorSwitch)CONFIG_INIT_TASK_STACK_SIZE=8192 $(PreprocessorSwitch)CONFIG_KERNEL_RHINO=1 $(PreprocessorSwitch)CONFIG_KERNEL_WORKQUEUE=1 $(PreprocessorSwitch)CONFIG_NON_ADDRESS_FLASH=1 $(PreprocessorSwitch)CONFIG_SAL=1 $(PreprocessorSwitch)CONFIG_SAL_DEFAULT_INPUTMBOX_SIZE=16 $(PreprocessorSwitch)CONFIG_SAL_DEFAULT_OUTPUTMBOX_SIZE=8 $(PreprocessorSwitch)CONFIG_SUPPORT_TSPEND=1 $(PreprocessorSwitch)CONFIG_UART_RECV_BUF_SIZE=1024 $(PreprocessorSwitch)CONFIG_XIP=1   -mabi=ilp32 -mtune=e906 -march=rv32imacxtheade   -Os -g -Wall -ffunction-sections -fdata-sections -Wl,-zmax-page-size=1024 -Wno-address -Wno-unused-but-set-variable -Wno-main -DLV_CONF_INCLUDE_SIMPLE
 
CFLAGS   := $(PreprocessorSwitch)USE_AOS_HAL $(PreprocessorSwitch)USE_AOS $(PreprocessorSwitch)USE_AT $(PreprocessorSwitch)USE_CH2601_EVB $(PreprocessorSwitch)USE_CHIP_CH2601 $(PreprocessorSwitch)USE_CLI $(PreprocessorSwitch)USE_CSI $(PreprocessorSwitch)USE_DRIVERS $(PreprocessorSwitch)USE_DRV_SND_CH2601 $(PreprocessorSwitch)USE_DRV_WIFI_AT_W800 $(PreprocessorSwitch)USE_HAL_CSI $(PreprocessorSwitch)USE_KV $(PreprocessorSwitch)USE_LVGL $(PreprocessorSwitch)USE_MINIALSA $(PreprocessorSwitch)USE_NETMGR $(PreprocessorSwitch)USE_NEWLIB $(PreprocessorSwitch)USE_PARTITION $(PreprocessorSwitch)USE_RHINO_ARCH $(PreprocessorSwitch)USE_RHINO_PWRMGMT $(PreprocessorSwitch)USE_RHINO $(PreprocessorSwitch)USE_SAL $(PreprocessorSwitch)USE_SDK_CHIP_CH2601 $(PreprocessorSwitch)USE_SEC_CRYPTO $(PreprocessorSwitch)USE_ULOG $(PreprocessorSwitch)USE_USERVICE  -Wno-unused-function -Os -g -mno-ldr -mno-ext -mno-mula -mno-srri -mno-ldi -mno-addsl -mno-ldd -mno-rev -mno-condmv  -nostdlib -Wpointer-arith -Wall -Wl,-EL -ffunction-sections -fdata-sections -Os -g  $(PreprocessorSwitch)AOS_COMP_CLI=y $(PreprocessorSwitch)CLI_CONFIG_INBUF_SIZE=256 $(PreprocessorSwitch)CLI_CONFIG_MAX_ARG_NUM=16 $(PreprocessorSwitch)CLI_CONFIG_MAX_COMMANDS=64 $(PreprocessorSwitch)CLI_CONFIG_MAX_ONCECMD_NUM=4 $(PreprocessorSwitch)CLI_CONFIG_OUTBUF_SIZE=512 $(PreprocessorSwitch)CLI_CONFIG_TASK_PRIORITY=60 $(PreprocessorSwitch)CONFIG_ARCH_INTERRUPTSTACK=32768 $(PreprocessorSwitch)CONFIG_CHIP_ALKAID=1 $(PreprocessorSwitch)CONFIG_CLI=1 $(PreprocessorSwitch)CONFIG_CPU_E906=1 $(PreprocessorSwitch)CONFIG_CSI=\"csi2\" $(PreprocessorSwitch)CONFIG_CSI_V2=1 $(PreprocessorSwitch)CONFIG_DEBUG=1 $(PreprocessorSwitch)CONFIG_INIT_TASK_STACK_SIZE=8192 $(PreprocessorSwitch)CONFIG_KERNEL_RHINO=1 $(PreprocessorSwitch)CONFIG_KERNEL_WORKQUEUE=1 $(PreprocessorSwitch)CONFIG_NON_ADDRESS_FLASH=1 $(PreprocessorSwitch)CONFIG_SAL=1 $(PreprocessorSwitch)CONFIG_SAL_DEFAULT_INPUTMBOX_SIZE=16 $(PreprocessorSwitch)CONFIG_SAL_DEFAULT_OUTPUTMBOX_SIZE=8 $(PreprocessorSwitch)CONFIG_SUPPORT_TSPEND=1 $(PreprocessorSwitch)CONFIG_UART_RECV_BUF_SIZE=1024 $(PreprocessorSwitch)CONFIG_XIP=1   -mabi=ilp32 -mtune=e906 -march=rv32imacxtheade   -mno-ldr -mno-ext -mno-mula -mno-srri -mno-ldi -mno-addsl -mno-ldd -mno-rev -mno-condmv
 
ASFLAGS  := $(PreprocessorSwitch)USE_AOS_HAL $(PreprocessorSwitch)USE_AOS $(PreprocessorSwitch)USE_AT $(PreprocessorSwitch)USE_CH2601_EVB $(PreprocessorSwitch)USE_CHIP_CH2601 $(PreprocessorSwitch)USE_CLI $(PreprocessorSwitch)USE_CSI $(PreprocessorSwitch)USE_DRIVERS $(PreprocessorSwitch)USE_DRV_SND_CH2601 $(PreprocessorSwitch)USE_DRV_WIFI_AT_W800 $(PreprocessorSwitch)USE_HAL_CSI $(PreprocessorSwitch)USE_KV $(PreprocessorSwitch)USE_LVGL $(PreprocessorSwitch)USE_MINIALSA $(PreprocessorSwitch)USE_NETMGR $(PreprocessorSwitch)USE_NEWLIB $(PreprocessorSwitch)USE_PARTITION $(PreprocessorSwitch)USE_RHINO_ARCH $(PreprocessorSwitch)USE_RHINO_PWRMGMT $(PreprocessorSwitch)USE_RHINO $(PreprocessorSwitch)USE_SAL $(PreprocessorSwitch)USE_SDK_CHIP_CH2601 $(PreprocessorSwitch)USE_SEC_CRYPTO $(PreprocessorSwitch)USE_ULOG $(PreprocessorSwitch)USE_USERVICE    $(PreprocessorSwitch)AOS_COMP_CLI=y $(PreprocessorSwitch)CLI_CONFIG_INBUF_SIZE=256 $(PreprocessorSwitch)CLI_CONFIG_MAX_ARG_NUM=16 $(PreprocessorSwitch)CLI_CONFIG_MAX_COMMANDS=64 $(PreprocessorSwitch)CLI_CONFIG_MAX_ONCECMD_NUM=4 $(PreprocessorSwitch)CLI_CONFIG_OUTBUF_SIZE=512 $(PreprocessorSwitch)CLI_CONFIG_TASK_PRIORITY=60 $(PreprocessorSwitch)CONFIG_ARCH_INTERRUPTSTACK=32768 $(PreprocessorSwitch)CONFIG_CHIP_ALKAID=1 $(PreprocessorSwitch)CONFIG_CLI=1 $(PreprocessorSwitch)CONFIG_CPU_E906=1 $(PreprocessorSwitch)CONFIG_CSI=\"csi2\" $(PreprocessorSwitch)CONFIG_CSI_V2=1 $(PreprocessorSwitch)CONFIG_DEBUG=1 $(PreprocessorSwitch)CONFIG_INIT_TASK_STACK_SIZE=8192 $(PreprocessorSwitch)CONFIG_KERNEL_RHINO=1 $(PreprocessorSwitch)CONFIG_KERNEL_WORKQUEUE=1 $(PreprocessorSwitch)CONFIG_NON_ADDRESS_FLASH=1 $(PreprocessorSwitch)CONFIG_SAL=1 $(PreprocessorSwitch)CONFIG_SAL_DEFAULT_INPUTMBOX_SIZE=16 $(PreprocessorSwitch)CONFIG_SAL_DEFAULT_OUTPUTMBOX_SIZE=8 $(PreprocessorSwitch)CONFIG_SUPPORT_TSPEND=1 $(PreprocessorSwitch)CONFIG_UART_RECV_BUF_SIZE=1024 $(PreprocessorSwitch)CONFIG_XIP=1   -mabi=ilp32 -mtune=e906 -march=rv32imacxtheade    


Objects0=$(IntermediateDirectory)/init_cli_cmd$(ObjectSuffix) $(IntermediateDirectory)/init_debug_cmd$(ObjectSuffix) $(IntermediateDirectory)/init_init$(ObjectSuffix) $(IntermediateDirectory)/lvgl_porting_oled$(ObjectSuffix) $(IntermediateDirectory)/src_main$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all
all: $(IntermediateDirectory)/$(OutputFile)

$(IntermediateDirectory)/$(OutputFile):  $(Objects) Always_Link 
	$(LinkerName) $(OutputSwitch) $(IntermediateDirectory)/$(OutputFile)$(ExeSuffix) $(LinkerNameoption) $(LinkOtherFlagsOption)  @$(ObjectsFileList)  $(LinkOptions) $(LibPath) $(Libs)
	$(OBJCOPY) $(ObjcopySwitch) $(ProjectPath)/$(IntermediateDirectory)/$(OutputFile)$(ExeSuffix)  $(ProjectPath)/Obj/$(OutputFile)$(IHexSuffix) 
	$(OBJDUMP) $(ObjdumpSwitch) $(ProjectPath)/$(IntermediateDirectory)/$(OutputFile)$(ExeSuffix)  > $(ProjectPath)/Lst/$(OutputFile)$(DisassemSuffix) 
	@echo size of target:
	@$(SIZE) $(ProjectPath)$(IntermediateDirectory)/$(OutputFile)$(ExeSuffix) 
	@echo -n checksum value of target:  
	@$(CHECKSUM) $(ProjectPath)/$(IntermediateDirectory)/$(OutputFile)$(ExeSuffix) 
	@$(ProjectName).modify.bat $(IntermediateDirectory) $(OutputFile)$(ExeSuffix) 

Always_Link:


##
## Objects
##
$(IntermediateDirectory)/init_cli_cmd$(ObjectSuffix): app/src/init/cli_cmd.c  
	$(CC) $(SourceSwitch) app/src/init/cli_cmd.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/init_cli_cmd$(ObjectSuffix) -MF$(IntermediateDirectory)/init_cli_cmd$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/init_cli_cmd$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/init_cli_cmd$(PreprocessSuffix): app/src/init/cli_cmd.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/init_cli_cmd$(PreprocessSuffix) app/src/init/cli_cmd.c

$(IntermediateDirectory)/init_debug_cmd$(ObjectSuffix): app/src/init/debug_cmd.c  
	$(CC) $(SourceSwitch) app/src/init/debug_cmd.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/init_debug_cmd$(ObjectSuffix) -MF$(IntermediateDirectory)/init_debug_cmd$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/init_debug_cmd$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/init_debug_cmd$(PreprocessSuffix): app/src/init/debug_cmd.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/init_debug_cmd$(PreprocessSuffix) app/src/init/debug_cmd.c

$(IntermediateDirectory)/init_init$(ObjectSuffix): app/src/init/init.c  
	$(CC) $(SourceSwitch) app/src/init/init.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/init_init$(ObjectSuffix) -MF$(IntermediateDirectory)/init_init$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/init_init$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/init_init$(PreprocessSuffix): app/src/init/init.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/init_init$(PreprocessSuffix) app/src/init/init.c

$(IntermediateDirectory)/lvgl_porting_oled$(ObjectSuffix): app/src/lvgl_porting/oled.c  
	$(CC) $(SourceSwitch) app/src/lvgl_porting/oled.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/lvgl_porting_oled$(ObjectSuffix) -MF$(IntermediateDirectory)/lvgl_porting_oled$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/lvgl_porting_oled$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/lvgl_porting_oled$(PreprocessSuffix): app/src/lvgl_porting/oled.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/lvgl_porting_oled$(PreprocessSuffix) app/src/lvgl_porting/oled.c

$(IntermediateDirectory)/src_main$(ObjectSuffix): app/src/main.c  
	$(CC) $(SourceSwitch) app/src/main.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/src_main$(ObjectSuffix) -MF$(IntermediateDirectory)/src_main$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/src_main$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/src_main$(PreprocessSuffix): app/src/main.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/src_main$(PreprocessSuffix) app/src/main.c


-include $(IntermediateDirectory)/*$(DependSuffix)
