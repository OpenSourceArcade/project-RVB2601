# 摩尔斯电码打字机

> 摩尔斯电码打字机，基于官方示例ch2601_gui_demo修改而成。

ch2601_gui_demo提供TFT LCD屏幕的开发演示。该程序通过调用开源lvgl组件实现屏幕label控件显示功能，程序内还包含lvgl组件的移植程序源码。

屏幕采用WiseChip公司的128x64像素规格的单彩色屏幕，参考链接地址：[https://www.wisechip.com.tw/zh-tw](https://www.wisechip.com.tw/zh-tw)。

屏幕控制器采用solomon-systech公司的SSD1309， 参考链接地址：[https://www.solomon-systech.com/en/product/advanced-display/oled-display-driver-ic/ssd1309/](https://www.solomon-systech.com/en/product/advanced-display/oled-display-driver-ic/ssd1309/)。

ch2601开发板采用单彩色图形显示面板，屏幕分辨率128x64 pixel，屏幕背景可选，该程序中采用的是一块黄色背景的屏幕。屏幕控制器采用SSD1309，通过4 wire SPI接口与主芯片连接, 对应的pin引脚分别为PA27、PA28、PA29、PA30.

该程序通过调用开源组件lvgl实现屏幕绘制功能，lvgl是一个免费开源的图形库，提供嵌入式系统的GUI能力，该开源库有使用方便，画面美观，内存占用率低等优点，lvgl的链接地址 https://lvgl.io/

# 莫尔斯电码A-Z，0-9：

![](code.jpg)

# 图片介绍

开机画面

![](start.jpg)

输入模式

![](input.jpg)

游戏模式（游戏中）

![](playing.jpg)

游戏模式（游戏结束）

![](gameover.jpg)